﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shop.Entities;
using Shop.Entities.Discounts;
using Shop.Types;

namespace ShopTests
{
    [TestClass]
    public class Tests
    {
        private List<Product> _products;

        [TestInitialize]
        public void TestInit()
        {
            _products = new List<Product>
            {

                new Product(PtoductType.A, "A", 100),
                new Product(PtoductType.B, "B", 100),
                new Product(PtoductType.C, "C", 100),
                new Product(PtoductType.D, "D", 100),
                new Product(PtoductType.E, "E", 100),
                new Product(PtoductType.F, "F", 100),
                new Product(PtoductType.G, "G", 100),
                new Product(PtoductType.H, "H", 100),
                new Product(PtoductType.I, "I", 100),
                new Product(PtoductType.J, "J", 100),
                new Product(PtoductType.K, "K", 100),
                new Product(PtoductType.L, "L", 100),
                new Product(PtoductType.M, "M", 100),
            };
        }

        [TestMethod]
        public void TestFirstDiscount_Success()
        {
            var cart = new Cart(_products, new List<IDiscount> { new FirstDiscount() });

            cart.ApplyDiscounts();
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.A && item.ApplyedDiscount == "First"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.B && item.ApplyedDiscount == "First"));
            Assert.AreEqual( cart.CalculateTotalPrice(), 1280);
        }


        [TestMethod]
        public void TestSecondDiscount_Success()
        {
            var cart = new Cart(_products, new List<IDiscount> { new SecondDiscount() });

            cart.ApplyDiscounts();
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.D && item.ApplyedDiscount == "Second"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.E && item.ApplyedDiscount == "Second"));
            Assert.AreEqual(cart.CalculateTotalPrice(), 1290);
        }

        [TestMethod]
        public void TestThirdDiscount_Success()
        {
            var cart = new Cart(_products, new List<IDiscount> { new ThirdDiscount() });

            cart.ApplyDiscounts();
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.E && item.ApplyedDiscount == "Third"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.F && item.ApplyedDiscount == "Third"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.G && item.ApplyedDiscount == "Third"));

            Assert.AreEqual(cart.CalculateTotalPrice(), 1285);
        }

        [TestMethod]
        public void TestFourthDiscount_Success()
        {
            var cart = new Cart(_products, new List<IDiscount> { new FourthDiscount() });

            cart.ApplyDiscounts();
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.A && item.ApplyedDiscount == "Fourth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.K && item.ApplyedDiscount == "Fourth"));

            Assert.AreEqual(cart.CalculateTotalPrice(), 1290);
        }

        [TestMethod]
        public void TestFifthDiscount_Success()
        {
            var cart = new Cart(_products, new List<IDiscount> { new FifthDiscount() });

            cart.ApplyDiscounts();
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.B && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.D && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.E && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.F && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.G && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.H && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.I && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.J && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.K && item.ApplyedDiscount == "Fifth"));

            Assert.AreEqual(cart.CalculateTotalPrice(), 1255);
        }

        [TestMethod]
        public void TestSixthDiscount_Success()
        {
            var cart = new Cart(_products, new List<IDiscount> { new SixthDiscount() });

            cart.ApplyDiscounts();
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.B && item.ApplyedDiscount == "Sixth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.D && item.ApplyedDiscount == "Sixth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.E && item.ApplyedDiscount == "Sixth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.F && item.ApplyedDiscount == "Sixth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.G && item.ApplyedDiscount == "Sixth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.H && item.ApplyedDiscount == "Sixth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.I && item.ApplyedDiscount == "Sixth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.J && item.ApplyedDiscount == "Sixth"));

            Assert.AreEqual(cart.CalculateTotalPrice(), 1220);
        }

        [TestMethod]
        public void TestSeventhDiscount_Success()
        {
            var cart = new Cart(_products, new List<IDiscount> { new SeventhDiscount() });

            cart.ApplyDiscounts();
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.B && item.ApplyedDiscount == "Seventh"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.D && item.ApplyedDiscount == "Seventh"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.E && item.ApplyedDiscount == "Seventh"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.F && item.ApplyedDiscount == "Seventh"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.G && item.ApplyedDiscount == "Seventh"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.H && item.ApplyedDiscount == "Seventh"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.I && item.ApplyedDiscount == "Seventh"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.J && item.ApplyedDiscount == "Seventh"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.K && item.ApplyedDiscount == "Seventh"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.L && item.ApplyedDiscount == "Seventh"));

            Assert.AreEqual(cart.CalculateTotalPrice(), 1100);
        }

        [TestMethod]
        public void TestBirthdayDiscount_Success()
        {
            var cart = new Cart(_products, new List<IDiscount> { new BirthdayDiscount( DateTime.Today) });

            cart.ApplyDiscounts();
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.A && item.ApplyedDiscount == "Birthday"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.B && item.ApplyedDiscount == "Birthday"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.C && item.ApplyedDiscount == "Birthday"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.D && item.ApplyedDiscount == "Birthday"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.E && item.ApplyedDiscount == "Birthday"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.F && item.ApplyedDiscount == "Birthday"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.G && item.ApplyedDiscount == "Birthday"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.H && item.ApplyedDiscount == "Birthday"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.I && item.ApplyedDiscount == "Birthday"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.J && item.ApplyedDiscount == "Birthday"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.K && item.ApplyedDiscount == "Birthday"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.L && item.ApplyedDiscount == "Birthday"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.M && item.ApplyedDiscount == "Birthday"));

            Assert.AreEqual(cart.CalculateTotalPrice(), 1040);
        }

        [TestMethod]
        public void TestAllDiscounts1_Success()
        {
            var cart = new Cart(_products, new List<IDiscount>
            {
                new FirstDiscount(),
                new SecondDiscount(),
                new ThirdDiscount(),
                new FourthDiscount(),
                new FifthDiscount(),
                new SixthDiscount(),
                new SeventhDiscount(),
                new BirthdayDiscount( DateTime.Today)
            });

            cart.ApplyDiscounts();
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.A && item.ApplyedDiscount == "First"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.B && item.ApplyedDiscount == "First"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.C && item.ApplyedDiscount == "Birthday"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.D && item.ApplyedDiscount == "Second"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.E && item.ApplyedDiscount == "Second"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.F && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.G && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.H && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.I && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.J && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.K && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.L && item.ApplyedDiscount == "Birthday"));

            Assert.AreEqual(cart.CalculateTotalPrice(), 1180);
        }

        [TestMethod]
        public void TestAllDiscounts2_Success()
        {
            var cart = new Cart(_products, new List<IDiscount>
            {
                new FirstDiscount(),
                new SecondDiscount(),
                new ThirdDiscount(),
                new FourthDiscount(),
                new FifthDiscount(),
                new SixthDiscount(),
                new SeventhDiscount(),
                new BirthdayDiscount( new DateTime(1999, 01, 21))
            });

            cart.ApplyDiscounts();
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.A && item.ApplyedDiscount == "First"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.B && item.ApplyedDiscount == "First"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.D && item.ApplyedDiscount == "Second"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.E && item.ApplyedDiscount == "Second"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.F && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.G && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.H && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.I && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.J && item.ApplyedDiscount == "Fifth"));
            Assert.IsNotNull(cart.Content.FirstOrDefault(item => item.ProductType == PtoductType.K && item.ApplyedDiscount == "Fifth"));

            Assert.AreEqual(cart.CalculateTotalPrice(), 1240);
        }
    }
}
