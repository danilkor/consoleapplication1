﻿using System;
using System.Collections.Generic;
using Shop.Entities;
using Shop.Entities.Discounts;
using Shop.Utils;

namespace Shop
{
    public class Program
    {
        static void Main()
        {
            var goods = ProductsInitializer.Init();
            var discounts = new List<IDiscount>
            {
                new FirstDiscount(),
                new SecondDiscount(),
                new ThirdDiscount(),
                new FourthDiscount(),
                new FifthDiscount(),
                new SixthDiscount(),
                new SeventhDiscount(),
                new BirthdayDiscount(new DateTime(1994, 09, 08))
            };

            var cart = new Cart(goods, discounts) as ICart;

            cart.ApplyDiscounts();

            var totalPrice = cart.CalculateTotalPrice();
            Console.WriteLine($"TotalPrice: {totalPrice}");
        }
    }
}