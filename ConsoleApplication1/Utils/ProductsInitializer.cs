﻿using System.Collections.Generic;
using Shop.Entities;
using Shop.Types;

namespace Shop.Utils
{
    public static class ProductsInitializer
    {
        public static List<Product> Init()
        {
            return new List<Product>
            {
                new Product(PtoductType.A, "A", 100),
                new Product(PtoductType.B, "B", 100),
                new Product(PtoductType.C, "C", 100),
                new Product(PtoductType.D, "D", 100),
                new Product(PtoductType.E, "E", 100),
                new Product(PtoductType.F, "F", 100),
                new Product(PtoductType.G, "G", 100),
                new Product(PtoductType.H, "H", 100),
                new Product(PtoductType.I, "I", 100),
                new Product(PtoductType.J, "J", 100),
                new Product(PtoductType.K, "K", 100),
                new Product(PtoductType.L, "L", 100),
                new Product(PtoductType.M, "M", 100),
            };
        }
    }
}
