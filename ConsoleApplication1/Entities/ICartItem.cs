﻿namespace Shop.Entities
{
    public interface ICartItem : IProduct
    {
        string ApplyedDiscount { get; set; }
        decimal DiscounntSizeInPercent { get; set; }
    }
}