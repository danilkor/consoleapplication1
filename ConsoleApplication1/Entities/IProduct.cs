﻿using Shop.Types;

namespace Shop.Entities
{
    public interface IProduct
    {
        decimal Price { get; set; }
        PtoductType ProductType { get; set; }
        string GoodName { get; set; }
    }
}
