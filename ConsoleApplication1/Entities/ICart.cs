﻿using System.Collections.Generic;
using Shop.Entities.Discounts;

namespace Shop.Entities
{
    public interface ICart
    {
        void Add( List<Product> goods );
        void Add( Product good );
        void ApplyDiscounts();
        decimal CalculateTotalPrice();
        List<IDiscount> CartDiscounts { get; }
        List<ICartItem> Content { get; set; }
    }
}