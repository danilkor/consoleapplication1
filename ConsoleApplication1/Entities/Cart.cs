﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shop.Entities.Discounts;

namespace Shop.Entities
{
    public class Cart : ICart
    {
        public List<ICartItem> Content { get; set; }
        public List<IDiscount> CartDiscounts { get; private set; }
        
        public Cart( List<IDiscount> discounts)
        {

            InitDiscounts(discounts);
            Content = new List<ICartItem>();
        }

        public Cart(ICartItem good, List<IDiscount> discounts)
        {
            InitDiscounts(discounts);
            Content = new List<ICartItem>
            {
                good
            };
        }

        public Cart(List<Product> content, List<IDiscount> discounts)
        {
            InitDiscounts(discounts);

            if ( content == null )
            {
                throw new ArgumentException();
            }
            Content = content.Select( item => item as ICartItem ).ToList();
        }

        public void Add( Product good )
        {
            Content.Add( good );
        }

        public void Add( List<Product> goods )
        {
            if ( goods == null )
            {
                throw new ArgumentException();
            }

            foreach ( var good in goods )
            {
                Content.Add( good );
            }
        }

        public decimal CalculateTotalPrice()
        {
            decimal totalPrice = 0;

            Content.ForEach( item => totalPrice += item.Price - item.Price / 100 * item.DiscounntSizeInPercent );
            return totalPrice;
        }

        public void ApplyDiscounts()
        {
            CartDiscounts.ForEach(item => item.Apply(this));
        }

        private void InitDiscounts(List<IDiscount> discounts)
        {
            if (discounts == null)
            {
                throw new Exception();
            }
            CartDiscounts = discounts;
        }
    }
}
