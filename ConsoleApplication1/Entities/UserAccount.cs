﻿using System;

namespace Shop.Entities
{
    public class UserAccount
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime Birthday { get; set; }
    }
}
