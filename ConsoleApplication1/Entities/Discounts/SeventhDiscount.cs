﻿using System.Collections.Generic;
using Shop.Types;

namespace Shop.Entities.Discounts
{
   public  class SeventhDiscount : BaseDiscount, IDiscount
    {
        private const int CartItemLimitForDiscount = 5;

        public SeventhDiscount() : base("Seventh", 20)
        {
        }

        public void Apply( Cart cart )
        {
            var productsNumber = GetCartItemsNumberWithoudDiscount( cart, true, PtoductType.A, PtoductType.C );
            var productsNumberInGroups = productsNumber - productsNumber % CartItemLimitForDiscount;

            SetProductsDiscountSizeinPercent( cart, productsNumberInGroups, true, PtoductType.A, PtoductType.C );
        }
    }
}
