﻿using System.Linq;
using Shop.Types;

namespace Shop.Entities.Discounts
{
    public abstract class BaseDiscount
    {
        protected readonly string DiscountName;
        protected readonly decimal DiscountSizeinPercent;

        protected BaseDiscount( string discountName, decimal discountSizeinPercent )
        {
            DiscountName = discountName;
            DiscountSizeinPercent = discountSizeinPercent;
        }

        protected int GetCartItemsNumberWithoudDiscount( Cart cart, params PtoductType[] products )
        {
            return GetCartItemsNumberWithoudDiscount( cart, false, products );
        }

        protected int GetCartItemsNumberWithoudDiscount( Cart cart, bool isExceptFor, params PtoductType[] products )
        {
            return cart.Content.Count( item => IsCartItemWithoutDiscount( item, isExceptFor, products ) );
        }

        protected void SetProductsDiscountSizeinPercent( Cart cart, int pairNumber, params PtoductType[] products )
        {
            SetProductsDiscountSizeinPercent( cart, pairNumber, false, products );
        }

        protected void SetProductsDiscountSizeinPercent( Cart cart, int pairNumber, bool isExceptFor, params PtoductType[] products )
        {
            cart.Content
                .Where( item => IsCartItemWithoutDiscount( item, isExceptFor, products ) )
                .Take( pairNumber )
                .ToList()
                .ForEach( item =>
                 {
                     item.DiscounntSizeInPercent = DiscountSizeinPercent;
                     item.ApplyedDiscount = DiscountName;
                 } );
        }

        private bool IsCartItemWithoutDiscount( ICartItem item, bool isExceptFor, params PtoductType[] products )
        {
            if ( !isExceptFor )
            {
                return products.Contains( item.ProductType ) && string.IsNullOrEmpty(item.ApplyedDiscount);

            }
            return !products.Contains( item.ProductType ) && string.IsNullOrEmpty(item.ApplyedDiscount);
        }

    }
}
