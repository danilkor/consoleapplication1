﻿using System;
using System.Linq;
using Shop.Types;

namespace Shop.Entities.Discounts
{
    public class FirstDiscount : BaseDiscount, IDiscount
    {

        public FirstDiscount() : base( "First", 10 )
        {
        }

        public void Apply( Cart cart )
        {
            var firstProduct = GetCartItemsNumberWithoudDiscount( cart, PtoductType.A );
            var secondProduct = GetCartItemsNumberWithoudDiscount( cart, PtoductType.B );

            var pairNumber = Math.Min( firstProduct, secondProduct );

            SetProductsDiscountSizeinPercent( cart, pairNumber, PtoductType.A );
            SetProductsDiscountSizeinPercent( cart, pairNumber, PtoductType.B );
        }
    }
}
