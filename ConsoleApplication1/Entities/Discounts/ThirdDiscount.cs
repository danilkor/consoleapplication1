﻿using System;
using Shop.Types;

namespace Shop.Entities.Discounts
{
    public class ThirdDiscount : BaseDiscount, IDiscount
    {
        public ThirdDiscount() : base( "Third", 5 )
        {
        }

        public void Apply( Cart cart )
        {
            var firstProduct = GetCartItemsNumberWithoudDiscount( cart, PtoductType.E );
            var secondProduct = GetCartItemsNumberWithoudDiscount( cart, PtoductType.F );
            var thirdProduct = GetCartItemsNumberWithoudDiscount( cart, PtoductType.G );

            var pairNumber = Math.Min( Math.Min( firstProduct, secondProduct ), thirdProduct );

            SetProductsDiscountSizeinPercent( cart, pairNumber, PtoductType.E );
            SetProductsDiscountSizeinPercent( cart, pairNumber, PtoductType.F );
            SetProductsDiscountSizeinPercent( cart, pairNumber, PtoductType.G );
        }
    }
}
