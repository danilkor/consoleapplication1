﻿using System;

namespace Shop.Entities.Discounts
{
    public class BirthdayDiscount : BaseDiscount, IDiscount
    {
        private readonly DateTime _birthdayDate;
        public BirthdayDiscount( DateTime birthdayDate ) : base("Birthday", 20)
        {
            _birthdayDate = birthdayDate;
        }

        public void Apply(Cart cart)
        {
            if (_birthdayDate != DateTime.Today)
            {
                return;
            }

            var itemsNumberWithoudDiscount = GetCartItemsNumberWithoudDiscount( cart, true );
            SetProductsDiscountSizeinPercent( cart, itemsNumberWithoudDiscount, true );
        }
    }
}