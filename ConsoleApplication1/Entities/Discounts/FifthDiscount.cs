﻿using Shop.Types;

namespace Shop.Entities.Discounts
{
    public class FifthDiscount : BaseDiscount, IDiscount
    {
        private const int CartItemLimitForDiscount = 3;
        public FifthDiscount() : base("Fifth", 5)
        {
        }

        public void Apply(Cart cart)
        {
            var productsNumber = GetCartItemsNumberWithoudDiscount(cart, true, PtoductType.A, PtoductType.C);
            var productsNumberInGroups = productsNumber - productsNumber % CartItemLimitForDiscount;
            
            SetProductsDiscountSizeinPercent( cart, productsNumberInGroups, true, PtoductType.A, PtoductType.C );
        }
    }
}
