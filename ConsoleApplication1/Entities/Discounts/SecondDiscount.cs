﻿using System;
using Shop.Types;

namespace Shop.Entities.Discounts
{
    public class SecondDiscount : BaseDiscount, IDiscount
    {
        public SecondDiscount() : base( "Second", 5 )
        {
        }

        public void Apply( Cart cart )
        {
            var firstProduct = GetCartItemsNumberWithoudDiscount( cart, PtoductType.D );
            var secondProduct = GetCartItemsNumberWithoudDiscount( cart, PtoductType.E );

            var pairNumber = Math.Min( firstProduct, secondProduct );

            SetProductsDiscountSizeinPercent( cart, pairNumber, PtoductType.D );
            SetProductsDiscountSizeinPercent( cart, pairNumber, PtoductType.E );
        }
    }
}