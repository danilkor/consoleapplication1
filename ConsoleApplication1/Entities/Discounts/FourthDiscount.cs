﻿using System;
using Shop.Types;

namespace Shop.Entities.Discounts
{
    public class FourthDiscount : BaseDiscount, IDiscount
    {
        public FourthDiscount() : base( "Fourth", 5 )
        {
        }

        public void Apply( Cart cart )
        {
            var firstProductNumber = GetCartItemsNumberWithoudDiscount( cart, PtoductType.A );
            var secondProductsNumber = GetCartItemsNumberWithoudDiscount( cart, PtoductType.K, PtoductType.L, PtoductType.M );

            var pairNumber = Math.Min( firstProductNumber, secondProductsNumber );

            SetProductsDiscountSizeinPercent( cart, pairNumber, PtoductType.A );
            SetProductsDiscountSizeinPercent( cart, pairNumber, PtoductType.K, PtoductType.L, PtoductType.M );
        }
    }
}