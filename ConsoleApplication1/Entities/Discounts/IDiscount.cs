﻿namespace Shop.Entities.Discounts
{
    public interface IDiscount
    {
        void Apply(Cart cart);
    }
}
