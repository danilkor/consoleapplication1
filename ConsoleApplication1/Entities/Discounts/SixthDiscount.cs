﻿using System.Collections.Generic;
using Shop.Types;

namespace Shop.Entities.Discounts
{
    public class SixthDiscount : BaseDiscount, IDiscount
    {
        private const int CartItemLimitForDiscount = 4;

        public SixthDiscount() : base("Sixth", 10)
        {
        }

        public void Apply( Cart cart )
        {
            var productsNumber = GetCartItemsNumberWithoudDiscount( cart, true, PtoductType.A, PtoductType.C );
            var productsNumberInGroups = productsNumber - productsNumber % CartItemLimitForDiscount;

            SetProductsDiscountSizeinPercent( cart, productsNumberInGroups, true, PtoductType.A, PtoductType.C );
        }
    }
}
