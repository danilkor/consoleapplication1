﻿using Shop.Types;

namespace Shop.Entities
{
    public class Product : ICartItem
    {
        public decimal Price { get; set; }
        public PtoductType ProductType { get; set; }
        public string GoodName { get; set; }
        public string ApplyedDiscount { get; set; }
        public decimal DiscounntSizeInPercent { get; set; }

        public Product(PtoductType productType, string goodName, decimal price)
        {
            Price = price;
            ProductType = productType;
            GoodName = goodName;
        }
    }
}
